# Import ssl - unneeded b/c of how spotify hosts its server, I think
import time
import json
from string import ascii_lowercase
from random import choice
# Check if requests installed
try:
    import requests
except ImportError:
    print("Module requires Requests library.")
    print("Install with 'pip install requests'.")
    exit()


# Disable dat warning
requests.packages.urllib3.disable_warnings()


class SpotifyController(object):
    """Create a controller for the spotify local server.

    Attributes:
        oauth_token: OAuth token in unknown encoding (likely URL-safe Base64).
            Retrieved from open.spotify.com/token. Sent with most requests.
        csrf_token: CSRF token in unknown encoding. Retrieved
            from local server. Sent with most requests.
        host: Address of local spotify server.
    """

    PORT = 4370
    DEFAULT_RETURN_ON = ['login', 'logout', 'play', 'pause', 'error', 'ap']
    ORIGIN_HEADER = {'Origin': 'https://open.spotify.com'}

    def __init__(self, host=None):
        """Accept optional host in format "xyz.com" e.g. 'spotilocal.com'.
        Defaults to 'spotilocal.com' (the application's default address).

        Additionally, fetches tokens to interact with local spotify server.
        """
        if host is None:
            self.host = "spotilocal.com"
        else:
            self.host = host
        self.oauth_token = self.get_oauth_token()
        self.csrf_token = self.get_csrf_token()

    def get_json(self, url, params={}, headers={}):
        """Return decoded json object from URL given optional params and
        headers in {key:value} form.
        """
        return requests.get(url, params=params, headers=headers,
                            verify=False).json()

    def generate_subdomain(self):
        """Generate a random subdomain replacing foo in 'foo.host.com'."""
        subdomain = ''.join(choice(ascii_lowercase) for x in range(10))
        return subdomain + "." + self.host

    def get_url(self, url):
        """Return full URL with port given domain suffix
        (e.g., '/service/version.json' will return 'xyz.host.com')
        """
        return "https://%s:%d%s" % (self.generate_subdomain(),
                                    SpotifyController.PORT, url)

    def get_version(self):
        """Return client version."""
        return self.get_json(self.get_url('/service/version.json'),
                            params={'service': 'remote'},
                            headers=SpotifyController.ORIGIN_HEADER)

    def get_oauth_token(self):
        """Return oauth token from 'open.spotify.com'."""
        return self.get_json('http://open.spotify.com/token')['t']

    def get_csrf_token(self):
        """Return csrf token from local server.
        Requires origin header to be set to generate the token.
        """
        return self.get_json(self.get_url('/simplecsrf/token.json'),
                            headers=SpotifyController.ORIGIN_HEADER)['token']

    def get_status(self, return_after=1,
                    return_on=DEFAULT_RETURN_ON):
        """Return the client status (playing, paused, stopped, et cetera)."""
        params = {
            'oauth': self.oauth_token,
            'csrf': self.csrf_token,
            'returnafter': return_after,
            'returnon': ','.join(return_on)
        }
        print json.dumps(self.get_json(self.get_url('/remote/status.json'),
                            params=params,
                            headers=SpotifyController.ORIGIN_HEADER),
                            sort_keys=True, indent=4, separators=(',', ': '))

    def pause(self, pause=True):
        """Sends json request to local server with 'pause' parameters."""
        params = {
            'oauth': self.oauth_token,
            'csrf': self.csrf_token,
            'pause': 'true' if pause else 'false'
        }
        self.get_json(self.get_url('/remote/pause.json'), params=params, 
                        headers=SpotifyController.ORIGIN_HEADER)

    def resume(self):
        """Sends json request to local server with 'pause' parameters."""
        self.pause(pause=False)

    def play(self, spotify_uri):
        """Sends json request to local server with spotify url in paramters
        for a particular song/album/playlist.
        """
        params = {
            'oauth': self.oauth_token,
            'csrf': self.csrf_token,
            'uri': spotify_uri,
            'context': spotify_uri,
        }
        self.get_json(self.get_url('/remote/play.json'), params=params,
                        headers=SpotifyController.ORIGIN_HEADER)


# MAIN
if __name__ == '__main__':
    s = SpotifyController()

    print "Playing~"
    s.play('spotify:user:spotify:playlist:4bWgWsz9p9eZVtpIvBgbsj')
    time.sleep(5)

    print "Pausing~"
    s.pause()
    time.sleep(2)

    print "Unpausing~"
    s.resume()
